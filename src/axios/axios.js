import axios from 'axios'

export const API_URL = process.env.VUE_APP_API_URL

export default axios.create({
    baseURL: API_URL,
    headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json;charset=utf-8'
    }
})