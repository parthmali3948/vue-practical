import Vue from "vue";
import Vuex from "vuex";

import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const state = () => ({
    btnloader: false,
    post_list: []
})

const actions = {

}

const mutations = {

    /** Update post list */
    update_post_list(state, data) {
        state.post_list = data;
    },
    /** */
    add_post(state, data) {
        state.post_list.push(data);
    },

    update_post(state, data) {
        if (data.index) {
            state.post_list[data.index] = data.objectdata;
        }
    },

    delete_post(state, index) {
        state.post_list.splice(index, 1);
    }

}


export default new Vuex.Store({
    state: state || {},
    mutations: mutations || {},
    actions: actions || {},
    modules: {},
    plugins: [createPersistedState()],
});