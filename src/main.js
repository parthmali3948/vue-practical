import Vue from 'vue';
import App from './App.vue';
import router from "./router.js";
import store from "./store";

Vue.config.productionTip = false

// VeeValidate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);


import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

/* import style css */
import '@/assets/css/style.css';

/* use axios */
import VueAxios from 'vue-axios';
import axios from './axios/axios.js';
Vue.use(VueAxios, axios)

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')