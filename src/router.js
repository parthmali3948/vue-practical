import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
        path: '',
        redirect: { name: 'List' }
    },
    {
        path: '/list',
        name: 'List',
        component: () =>
            import ('@/pages/list.vue')
    },
    {
        path: '/add',
        name: 'Add',
        component: () =>
            import ('@/pages/add.vue')
    },
    {
        path: '/edit/:id',
        name: 'Edit',
        component: () =>
            import ('@/pages/edit.vue')
    },
    {
        path: '*',
        name: '404',
        component: () =>
            import ('@/pages/404.vue')
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    routes
});

export default router;